using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnPoint : MonoBehaviour
{
    #region Variables

    [SerializeField] private Transform enemyPrefab;
    [SerializeField] private Enemy currentEnemy;

    #endregion

    public void SpawnEnemy()
    {
        // There should be poooling here, but no time
        currentEnemy = Instantiate(enemyPrefab, transform.position, transform.rotation).GetComponent<Enemy>();
    }

    public Enemy GetCurrentEnemy()
    {
        return currentEnemy;
    }
}
