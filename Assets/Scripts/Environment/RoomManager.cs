using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

public class RoomManager : MonoBehaviour
{
   #region Variables

   private int roomIndex;
   [SerializeField] private List<EnemySpawnPoint> spawnPointsList; 
   [SerializeField] private Door door;
   
   [ShowNonSerializedField] // for debug
   private int enemiesInRoom;
   
   #endregion

   public void Init(int roomIndex)
   {
      this.roomIndex = roomIndex;
      enemiesInRoom = spawnPointsList.Count;
   }

   

   public void StartRoom()
   {
      SpawnEnemies();
   }
   
   public void CompleteRoom()
   {
      door.OpenDoor();
      ClearRoom();
      LevelManager.Instance.RoomCompleted(roomIndex);
   }
   
   private void SpawnEnemies()
   {
      foreach (var spawnPoint in spawnPointsList)
      {
         spawnPoint.SpawnEnemy();
         spawnPoint.GetCurrentEnemy().OnDeath += OnEnemyDie;
      }
   }

   private void ClearRoom()
   {
      foreach (var spawnPoint in spawnPointsList)
      {
         //DESPAWN.
         spawnPoint.GetCurrentEnemy().OnDeath -= OnEnemyDie;
      }
   }
   


   /// <summary>
   /// When enemy die call this method t
   /// </summary>
   public void OnEnemyDie(Character character)
   {
      enemiesInRoom--;

      if (IsRoomComplete())
         CompleteRoom();
   }

   private bool IsRoomComplete()
   {
      if (enemiesInRoom == 0)
         return true;
      else
          return false;
   }


   #region Debug
   // Debug
   [ContextMenu("ForceCompleteRoom")]
   private void ForceCompleteRoom()
   {
      CompleteRoom();
   }

   [Button]
   private void KillAllEnemiesInRoom()
   {
      foreach (var spawnPoint in spawnPointsList)
      {
         spawnPoint.GetCurrentEnemy().KillCharacter(); 
      }
   }
   #endregion

}
