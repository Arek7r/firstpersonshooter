using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globals
{

}

public enum GameState
{
    Menu,
    Gameplay,
    Loading,
}

public enum UIType
{
    Menu,
    Gameplay,
    Loading,
}

public static class GlobalString
{
    public const string Player = "Player";
    public const string Enemy = "Enemy";
}

public class LayerMaskID
{
    /// <summary>
    /// Example:
    ///  if (tr.gameObject.layer == LayerMaskID.Default)
    /// </summary>
    public static readonly int Default = LayerMask.NameToLayer("Default");


    /// <summary>
    /// Example:
    /// if (Physics.Raycast(ray, out hit, distance, LayerMaskID.BitLayer.BigCarMask))
    /// </summary>
    public struct BitLayer
    {
        public static readonly int Default = 1 << LayerMask.NameToLayer("Default");
    }
}