using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

public class PlayerInput : CharacterInput
{
    #region Variables
    //private UnityEngine.InputSystem.PlayerInput input;
    
    [ShowNonSerializedField]
    private bool inited;
    private Character character;
    private CharacterEquipment equipment;
   
    [SerializeField] private InputActionReference moveInput;
    [SerializeField] private InputActionReference lookInput;
    [SerializeField] private InputActionReference useRightItem;

    #endregion

    public override void Init(Character character)
    {
        base.Init(character);
        useRightItem.action.started += UseRightItem;
        equipment = character.GetEquipment();
        inited = true;
    }

    public override void DeInit()
    {
        base.DeInit();
        useRightItem.action.started -= UseRightItem;
    }

    void Update()
    {
        if (inited == false )
            return;
        
        UpdateInput();
    }

    private void UpdateInput()
    {
        if (moveInput)
            moveDirection = moveInput.action.ReadValue<Vector2>();
        
        if (lookInput)
            lookDirection = lookInput.action.ReadValue<Vector2>();
    }
    
    // Input events
    private void UseRightItem(InputAction.CallbackContext obj)
    {
        //OnRightEqUse.Invoke();
        equipment.UseRightHandEq();
    }
    
}
