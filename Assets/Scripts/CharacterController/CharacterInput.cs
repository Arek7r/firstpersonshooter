using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

public class CharacterInput : MonoBehaviour
{
   [ShowNonSerializedField]
   protected Vector2 moveDirection;
   [ShowNonSerializedField]
   protected Vector2 lookDirection;
   
   public Action OnRightEqUse;

   public virtual void Init(Character character)
   {}

   public virtual void DeInit()
   {}

   public Vector2 GetMoveDirection()
   {
      return moveDirection;
   }
   
   public Vector2 GetLookDirection()
   {
      return lookDirection;
   }
}
