using System.Collections;
using System.Collections.Generic;
using MiniGames.Overtake.Scripts;
using UnityEngine;

/// <summary>
/// The rooms are activated one by one to divert resources
/// </summary>
public class LevelManager : Singleton<LevelManager>
{
    public Player player;
    public List<RoomManager> roomManagerList;
    
    
    /// <summary>
    /// Level after scene loaded starts here, prepare all step by step to avoid problems in future
    /// </summary>
    private void Start()
    {
        SetupPlayer();
        SetupUI();
        SetupRooms();
    }

    private void SetupPlayer()
    {
        // Spawn and setup player etc. now just from reference
        player.Init();
    }

    private void SetupUI()
    {
        // I assume that the game starts as soon as you load a lvl
        UIManager.Instance.SetUI(GameState.Gameplay);
    }
    
    private void SetupRooms()
    {
        // Automatic set number for room to easy navigate in future
        for (var index = 0; index < roomManagerList.Count; index++)
        {
            roomManagerList[index].Init(index);
        }

        StartRoom(0);
    }
    
    #region ManageRooms
  
    /// <summary>
    /// Starts specific room
    /// </summary>
    /// <param name="indexOfRoom"></param>
    public void StartRoom(int indexOfRoom)
    {
        // here should be a safety net here, clearing the previous lvl?
        
        if (indexOfRoom <= roomManagerList.Count)
            roomManagerList[indexOfRoom].StartRoom();
    }
    

    /// <summary>
    /// Called when room is completed and start next room
    /// </summary>
    /// <param name="roomIndex"></param>
    public void RoomCompleted(int roomIndex)
    {
        if (roomIndex + 1 < roomManagerList.Count)
        {
            roomIndex++;
            StartRoom(roomIndex);
        }
    }
    #endregion

}
