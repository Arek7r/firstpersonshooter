using System;
using System.Collections;
using System.Collections.Generic;
using MiniGames.Overtake.Scripts;
using NaughtyAttributes;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    #region Variables

    [SerializeField] private Transform menuCanvas;
    [SerializeField] private Transform hudCanvas;
    private Transform currentCanvas;

    #endregion

    public void SetUI(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.Menu:
                SetMenuUI();
                break;
            case GameState.Gameplay:
                SetGamePlayUI();
                break;
            case GameState.Loading:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(gameState), gameState, null);
        }
    }

    private void SetMenuUI(bool state = true)
    {
        if (menuCanvas == null)
            return;

        if (state == true)
        {
            DisableCurrentUI();
            menuCanvas.gameObject.SetActive(true);
            currentCanvas = menuCanvas;
        }
        else
        {
            menuCanvas.gameObject.SetActive(false);
        }
    }

    private void SetGamePlayUI(bool state = true)
    {
        if (hudCanvas == null) 
            return;

        if (state == true)
        {
            // if not using Phone Simulator
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            
            DisableCurrentUI();
            hudCanvas.gameObject.SetActive(true);
            currentCanvas = hudCanvas;
        }
        else
        {
            hudCanvas.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Disable current UI. Anims, fades etx 
    /// </summary>
    private void DisableCurrentUI()
    {
        if (currentCanvas)
            currentCanvas.gameObject.SetActive(false);
    }

    private void DisableAllUI()
    {
    }

    // Called from Gamemanager after loaded level
    public void SetActiveLoadingScreen(bool state)
    {
    }
}