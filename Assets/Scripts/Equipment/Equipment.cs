using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

public class Equipment : Item
{
    #region Variables
    [SerializeField]
    private float cooldownTime = 1f;
    [ShowNonSerializedField]
    private float cooldownTimer = 0.0f;  
    
    #endregion

    public override void Use()
    {
        if (IsReadyToUse() == false)
            return;

        PrepareCooldown();
    }

    public virtual bool IsReadyToUse()
    {
        // cooldown is not finish
        if (cooldownTimer > 0.0f)
        {
            return false;
        }
        
        return true;
    }

    protected void PrepareCooldown()
    {
        cooldownTimer = cooldownTime;
    }
    protected virtual void Update()
    {
        if (cooldownTimer > 0)
        {
            cooldownTimer -= Time.deltaTime;
        }
    }
}
