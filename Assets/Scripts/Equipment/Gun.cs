
using NaughtyAttributes;
using Pool;
using ScriptsNormal.Bullets_Projectiles;
using UnityEngine;

public class Gun : Equipment
{
    #region Variables

    [BeginGroup("References")]
    [EndGroup]    //it's small bug, waiting for fix from asset author   
    public Transform bulletSpawnPoint;
  
    // Bullet data
    [SerializeField] SimpleBullet simpleBulletPrefab;
    [SerializeField] private LayerMask bulletHitLayer = 1 << 0;
    [SerializeField] private float velocity = 380;
    [SerializeField] private int damage = 10;
    private SimpleBullet currBullet;
    private DamageStruct _damageInfo;
    
    //Ammo
    [SerializeField]
    private int ammoCountMax = 99999;
    [ShowNonSerializedField]
    private int ammoCountCurrent = 99999;
    private int ammoPerMagazine = 99999;

    // Reload
    [SerializeField]
    private bool autoReload = true;

    private bool isReloading;
    [SerializeField]
    private float reloadingTime = 2f;
    private float reloadingTimer = 0.0f;  

    #endregion

    protected virtual void Awake()
    {
        _damageInfo = new DamageStruct();
        UpdateDamageInfo();
    }
    
    protected override void Update()
    {
        base.Update();
        UpdateReloading();
    }

    private void UpdateReloading()
    {
        if (isReloading)
        {
            if (reloadingTimer > 0)
            {
                reloadingTimer -= Time.deltaTime;
            }
            else
            {
                ReloadEnd();
            }
        }
    }

    /// <summary>
    /// Use a gun
    /// </summary>
    public override void Use()
    {
        if (IsReadyToUse() == false)
        {
            if (IsOutOfAmmo())
                PlayOutOfAmmoEffects();

            return;
        }


        Shoot();
        PrepareCooldown();
    }

    /// <summary>
    /// Start shoot
    /// </summary>
    public virtual void Shoot()
    {
        currBullet = ObjectPoolManager.Instance.GetObjectAuto(simpleBulletPrefab.name, simpleBulletPrefab);
        currBullet.transform.position = bulletSpawnPoint.position;
        currBullet.transform.rotation = bulletSpawnPoint.rotation;
        
        currBullet.Init(_damageInfo);
        currBullet.gameObject.SetActive(true);
        
        ammoCountCurrent--;
        PlayShootEffects();
       
        if (autoReload && ammoCountCurrent <=0)
            ReloadStart();
    }

    /// <summary>
    /// Start process of realoding. Timer updates in Update
    /// </summary>
    private void ReloadStart()
    {
        reloadingTimer = reloadingTime;
        isReloading = true;
        PlayReloadStartEffects();
    }
    /// <summary>
    /// It's call on end reload from UpdateReloading()
    /// </summary>
    private void ReloadEnd()
    {
        isReloading = false;
        ammoCountCurrent = ammoCountMax;
        PlayReloadEndEffects();
    }

    private void UpdateDamageInfo()
    {
        _damageInfo.damage = damage;
        _damageInfo.velocity = velocity;
        _damageInfo.hitLayer = bulletHitLayer;
        _damageInfo.sender = transform;
    }
    
    #region Effects
    
    /// <summary>
    /// Play effects of shoot
    /// </summary>
    protected virtual void PlayShootEffects()
    { }

    
    /// <summary>
    /// Play effects of empty magazine
    /// </summary>
    protected virtual void PlayOutOfAmmoEffects()
    { }
    
    protected virtual void PlayReloadStartEffects()
    { }
    protected virtual void PlayReloadEndEffects()
    { }
    #endregion
    #region Checks
    /// <summary>
    /// Check if gun is ready to shoot
    /// </summary>
    public override bool IsReadyToUse()
    {
        return base.IsReadyToUse() 
               && IsOutOfAmmo() == false
               && isReloading == false;
    }

    /// <summary>
    /// Check if gun has ammo
    /// </summary>
    public bool IsOutOfAmmo()
    {
        return ammoCountCurrent <= 0;
    }
    #endregion

    [Button]
    private void DebugUse()
    {
        Use();
    }
}
