using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

public class CharacterEquipment : MonoBehaviour
{
    #region MyRegion
    [SerializeField] private Equipment rightHandEq;
    [SerializeField] private Equipment leftHandEq;

    #endregion


    public Equipment GetRightHandEq()
    {
        return rightHandEq;
    }

    [Button]
    public void UseRightHandEq()
    {
        rightHandEq.Use();
    }
}
