using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

public class Player : Character
{
    #region Variables

    // References
    [BoxGroup("References")]
    [SerializeField]
    private Transform cameraObject;

    [BoxGroup("Parameters")]
    public float cameraSpeed = 100f; 
    
    private float rotationByX = 0f;
    private float rotationByY = 0f; 
    //private float _axisY;
    //private float _axisX;
    
    #endregion

    public override void Init()
    {
        if (input)
            input.Init(this);
    }

   

    private void OnDestroy()
    {
        if (input)
            input.DeInit();
    }

    private void Update()
    {
        Look();
    }

    private void FixedUpdate()
    {
        Move();
    }

    protected override void Look()
    {
        // up/down
        rotationByX -= input.GetLookDirection().y * cameraSpeed * Time.deltaTime;
        // left/right
        rotationByY += input.GetLookDirection().x * cameraSpeed * Time.deltaTime; 
        
        // block rotation to avoid 180 - my concept
        rotationByX = Mathf.Clamp(rotationByX, -90f, 90f); 
        rotationByY = Mathf.Clamp(rotationByY, -90f, 90f); 
        
        // Rotate Camera
        cameraObject.localRotation = Quaternion.Euler(rotationByX, rotationByY, 0f);
    }
}