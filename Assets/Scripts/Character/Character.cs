using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Serialization;

public class Character : MonoBehaviour
{
    #region Variables

    [BoxGroup("References")]
    [SerializeField] protected CharacterInput input;
    [BoxGroup("References")]
    [SerializeField] protected CharacterEquipment characterEquipment;
    [BoxGroup("References")]
    [SerializeField] protected Rigidbody rb;
   
    [BoxGroup("Parameters")]
    [SerializeField] private int hpMax;
    [ShowNonSerializedField]
    private float hpCurrent;
    
    [BoxGroup("Parameters")]
    [SerializeField] protected  float moveSpeed = 5.0f;

    
    public event Action<Character> OnDeath;
    #endregion

    #region MonoBehaviour

    protected virtual void OnEnable()
    {
        hpCurrent = hpMax;
    }
    private void OnDestroy()
    {
        //Disconnect all connected methods
        OnDeath = null;
    }
    
    #endregion

    #region Base

    public virtual void Init()
    { }

    public virtual void TakeDamage(float value)
    {
        hpCurrent -= value;

        if (hpCurrent <= 0)
        {
            Die();
            hpCurrent = 0;
        }
    }
    
    protected virtual void Die()
    {
        OnDeath?.Invoke(this);
        gameObject.SetActive(false);
    }

    protected virtual void Move()
    {
        if (rb != null)
        {
            Vector3 movement = new Vector3(input.GetMoveDirection().x, 0.0f, input.GetMoveDirection().y) * moveSpeed * Time.fixedDeltaTime;
            rb.MovePosition(rb.position + movement);
        }
    }

    protected virtual void Look()
    { }

    #endregion
    
    
    #region Get
    public CharacterEquipment GetEquipment()
    {
        return characterEquipment;
    }
  
    #endregion

    [ContextMenu("KillCharacter")]
    public void KillCharacter()
    {
        TakeDamage(hpMax);
    }
}
